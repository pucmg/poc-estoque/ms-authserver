package br.com.pucminas.estoque.authserver.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.pucminas.estoque.authserver.model.request.AuthRequestDTO;
import br.com.pucminas.estoque.authserver.model.request.UserRequestDTO;
import br.com.pucminas.estoque.authserver.model.response.AuthResponseDTO;
import br.com.pucminas.estoque.authserver.service.AuthService;
import io.swagger.annotations.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1/auth")
@Api(tags = "AuthDocumentation")
public class AuthController {

    private AuthService authService;

    @Autowired
    AuthController(AuthService authService){
        this.authService = authService;
    }


    @ApiOperation(value = "Generate Token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", required = true, allowEmptyValue = false, paramType = "header", example = "colaborador"),
            @ApiImplicitParam(name = "password", required = true, allowEmptyValue = false, paramType = "header", example = "123"),
            @ApiImplicitParam(name = "grant_type", required = true, allowEmptyValue = false, paramType = "header", example = "info-register")
    })
    @GetMapping("/token")
    public ResponseEntity<?> generateToken(@RequestHeader("username") String username, @RequestHeader("password") String password, @RequestHeader("grant_type") String grant_type){
        AuthRequestDTO authRequestDTO = new AuthRequestDTO(new UserRequestDTO(username, password), grant_type);
        Optional<AuthResponseDTO> authResponseDTO = this.authService.generateToken(authRequestDTO);

        if (authResponseDTO.isPresent()){
            return ResponseEntity.ok().body(authResponseDTO.get());
        }{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @ApiOperation(value = "Validate Token")
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @GetMapping("/validate")
    public ResponseEntity<?> authorizeToken(@RequestHeader("authorization") String bearertoken){
        String[] token = bearertoken.replace(",", "").split(" ");
        boolean isAuthorized = this.authService.isAuthorized(token[1]);

        if(isAuthorized){
            return ResponseEntity.ok().build();
        }else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }


}
