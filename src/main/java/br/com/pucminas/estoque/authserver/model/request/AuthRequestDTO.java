package br.com.pucminas.estoque.authserver.model.request;

import br.com.pucminas.estoque.authserver.model.entities.AuthEntity;
import lombok.*;

@Data
@Getter
@Builder
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class AuthRequestDTO {

    @NonNull
    private UserRequestDTO userRequestDTO;

    @NonNull
    private String grantTypeEnum;

    private String token;

    public AuthEntity toEntity(){
        return new AuthEntity(
                this.grantTypeEnum,
                this.token
        );
    }

}
