package br.com.pucminas.estoque.authserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.pucminas.estoque.authserver.model.entities.UserEntity;
import br.com.pucminas.estoque.authserver.model.request.UserRequestDTO;
import br.com.pucminas.estoque.authserver.repository.UserRepository;

@Service
public class UserService {

    private UserRepository userRepo;

    @Autowired
    UserService(UserRepository userRepo){
        this.userRepo = userRepo;
    }

    public void createUser(UserRequestDTO userRequestDTO, String pocSecret){
    	if(pocSecret == "PUCMG") {
    		UserEntity userEntity = userRequestDTO.toEntity();

            this.userRepo.save(userEntity);
    	}
    }

}
