package br.com.pucminas.estoque.authserver.model.request;

import br.com.pucminas.estoque.authserver.model.entities.UserEntity;
import lombok.*;

@Data
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO {
    private String username;
    private String password;

    public UserEntity toEntity(){
        return new UserEntity(
                this.username,
                this.password
        );
    }
}
