package br.com.pucminas.estoque.authserver.model.response;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AuthResponseDTO {

    private String authorization_type = "Bearer Token";

    @NonNull
    private String token;
}
