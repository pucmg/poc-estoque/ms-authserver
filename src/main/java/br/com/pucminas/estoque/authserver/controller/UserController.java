package br.com.pucminas.estoque.authserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import br.com.pucminas.estoque.authserver.model.request.UserRequestDTO;
import br.com.pucminas.estoque.authserver.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1")
@Api(tags = "UserDocumentation")
public class UserController {

    private UserService userService;

    @Autowired
    UserController(UserService userService){
        this.userService = userService;
    }

    
    @ApiOperation(value = "Create user for test purpose")
    @ApiImplicitParam(name = "pocSecret", value = "Get secret code on UserService or request to the SysAdmin", required = true, allowEmptyValue = false, example = "secret code")
    @PostMapping("/user")
    public void createUser(@RequestBody UserRequestDTO userRequestDTO, String pocSecret){
        this.userService.createUser(userRequestDTO, pocSecret);

    }
}
