package br.com.pucminas.estoque.authserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.pucminas.estoque.authserver.model.entities.AuthEntity;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<AuthEntity, Long> {

    Optional<AuthEntity> getAuthByToken(String Token);
}
